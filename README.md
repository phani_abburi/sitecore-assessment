## Sitecore technical assessment (CMS Customer monitoring)

### Table of contents

* [Installation](#installation)
* [Gulp tasks](#gulp-tasks)
* [Credits](#credits)

### Installation

- Please make sure that nodejs is installed in your system. Check nodejs [Here](https://nodejs.org/en/).
- Please install **Gulp** and **Bower** for managing your task runners and dependency management globally by running this command on your command line interface.

  ```
  npm install -g gulp-cli bower
  ```

- Clone the repository by running this command on your command line interface

  ```
  git clone https://jetpumz@bitbucket.org/jetpumz/sitecore-assessment.git
  ```

- Go to the project directories and run these command for fetching the **Node dependencies** and **bower libraries**

  ```
  npm install && bower install
  ```

- Project should be ready to run, please run below command and open the project on ```http://localhost:9010```

  ```
  gulp server
  ```

### Gulp tasks

Below are several main tasks that this project has :

* ```default```
    * It will run a ```build``` gulp task.
* ```server```
    * Start a dev server using ```gulp-connect```, serving the app folder.
* ```server:dist```
    * Start a dev server using ```gulp-connect```, serving the distribution folder.
* ```build```
    * Build a distribution package. All of the packages are stored on distribution folder.

### Credits

Several frameworks that have been using to create this prototype are :

- Gulp
- Reactjs
- Pug
- Sass
- Twitter bootstrap
- d3
- webpack
- jsx

*Note that there is a folder jquery on app/vendors folder but I didn't use it, it's there because it was downloaded when I installed bootstrap