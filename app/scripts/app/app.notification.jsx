'use strict';

var React = require('react');

module.exports = React.createClass({
    displayName: 'notification',
    shouldComponentUpdate: function () {
        document.querySelector('.notification').className = "notification show";
        setTimeout(function () {
            document.querySelector('.notification').className = "notification";
        }, 5000)
    },
    render: function () {
        return (<div className="notification">You have new registered customer</div>);
    }
});
