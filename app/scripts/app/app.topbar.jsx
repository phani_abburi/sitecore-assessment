'use strict';

var React = require('react');

module.exports = React.createClass({
    displayName: 'TopBar',
    toggleOpen: function() {
        this.props.toggleOpen();
    },
    render: function () {
        return (
            <header className="top-bar">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="logo-container">
                                <img src="../assets/images/logo-sitecore.png" />
                            </div>
                            <div className="toolbar-container">
                                <ul className="toolbars">
                                    <li>
                                        <button className={`toolbar ${this.props.sideBarOpened ? 'active' : ''}`} onClick={this.toggleOpen.bind(this)}>
                                            <img src="../assets/images/chart.png" />
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
});
