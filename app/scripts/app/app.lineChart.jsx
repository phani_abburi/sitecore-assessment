'use strict';

var React = require('react');
var d3 = require('d3');

module.exports = React.createClass({
    displayName: 'LineChart',
    initializeLineChart: function () {

        var self = this;
        var containerWidth = document.querySelector('.line-chart').offsetWidth;
        var margin = {top: 10, right: 35, bottom: 20, left: 35},
            width = containerWidth - margin.left - margin.right,
            height = 300 - margin.top - margin.bottom;

        var x = d3.time.scale()
            .range([0, width]);

        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .tickFormat(function (v, i) {
                var data = ['2010', '2011', '2012', '2013', '2014', '2015'];
                return data[i];
            })
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left");

        var area = d3.svg.area()
            .x(function (d) {
                return x(d.year);
            })
            .y0(height)
            .y1(function (d) {
                return y(d.value);
            });

        var line = d3.svg.line()
            .x(function (d) {
                return x(d.year);
            })
            .y(function (d) {
                return y(d.value);
            });

        var svg = d3.select(".line-chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        d3.xml('datas.xml', function (xml) {
            var charts = xml.getElementsByTagName('chart');

            for (var i = 0, l = charts.length; i < l; i++) {
                var chart = charts[i];

                var year = chart.getAttribute('year');
                var value = chart.textContent;

                self.props.addChart({
                    year: year,
                    value: value
                });
            }

            x.domain(d3.extent(self.props.annualCharts, function (d) {
                return d.year;
            }));

            y.domain(d3.extent(self.props.annualCharts, function (d) {
                return d.value;
            }));

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("App usage");

            svg.append("path")
                .datum(self.props.annualCharts)
                .attr("class", "area")
                .attr("d", area)

        });

    },
    componentDidMount: function () {
        this.initializeLineChart();
    },
    render: function () {
        return (
            <div>
                <h4 className="title-section">Annual application usage report</h4>
                <div className="line-chart"></div>
            </div>
        );
    }
});
