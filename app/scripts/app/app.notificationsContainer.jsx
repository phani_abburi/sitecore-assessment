'use strict';

var React = require('react');
var Notification = require('./app.notification');

module.exports = React.createClass({
    displayName: 'notificationsContainer',
    render: function () {
        return (
            <div className="notifications-container">
                <div className="notifications">
                    <Notification mapDataFetched={this.props.mapDataFetched}/>
                </div>
            </div>
        );
    }
});
