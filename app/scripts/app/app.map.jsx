'use strict';

var React = require('react');
var IntervalMixin = require('../services/service.interval-mixin');
var d3 = require('d3');
var q = require('q');

module.exports = React.createClass({
    displayName: 'MapContainer',
    mixins: [IntervalMixin],
    addClickListener: function (e) {
        //TODO : remove this after it's done
        console.log(e.latLng.toString())
    },
    reverseGeocode: function (latLng) {
        var deferred = q.defer();
        var geocoder = new google.maps.Geocoder;
        latLng = new google.maps.LatLng(latLng.lat, latLng.lng);

        geocoder.geocode({'location': latLng}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                deferred.resolve(results);
            } else {
                deferred.reject(results);
            }
        });

        return deferred.promise;
    },
    addMarker: function (obj, map) {

        var latLng = new google.maps.LatLng(obj.lat, obj.lng);
        var marker = new google.maps.Marker({
            position: latLng,
            icon: '../assets/images/sitecorelogo32x32.png',
            animation: google.maps.Animation.DROP,
            properties: obj
        });

        var markerContent = '<h5><strong>' + obj.name + '</strong></h5>' +
            '<p>' + obj.address + '</p>' +
            '<p>' + obj.city + ', ' + obj.country + '</p>';

        var infoWindow = new google.maps.InfoWindow({
            content: markerContent
        });

        marker.addListener('click', function () {
            infoWindow.open(map, marker);
        });

        marker.setMap(map);
        this.props.addNewMarker(marker);
    },
    addCircleRadius: function (map) {
        var circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            map: map,
            center: map.getCenter(),
            radius: 80000
        });
    },
    getRandomMarkers: function () {

        var self = this;

        /**
         * used to generate random coordinates
         * */

        var generateRandomPointOnRadius = function (center, radius) {
            var x0 = center.lng;
            var y0 = center.lat;
            // Convert Radius from meters to degrees.
            var rd = radius / 111300;

            var u = Math.random();
            var v = Math.random();

            var w = rd * Math.sqrt(u);
            var t = 2 * Math.PI * v;
            var x = w * Math.cos(t);
            var y = w * Math.sin(t);

            var xp = x / Math.cos(y0);

            // Resulting point.
            return {'lat': y + y0, 'lng': xp + x0};
        };

        /**
         * used to find the coordinates city location
         * */

        var generateCity = function (records) {

            var adminArea = records.find(function (elm) {
                return elm.types.indexOf('administrative_area_level_2') != -1;
            });

            var city = adminArea.address_components.find(function (elm) {
                return elm.types.indexOf('administrative_area_level_2') != -1;
            });

            return city;

        };

        /**
         * used to find the coordinates address location
         * */

        var generateAddress = function (records) {

            var address = records.find(function (elm) {
                return elm.types.indexOf('route') != -1;
            });

            return address.formatted_address;

        };

        /**
         * used to find the coordinates country location
         * */

        var generateCountry = function (records) {

            var country = records.find(function (elm) {
                return elm.types.indexOf('country') != -1;
            });

            return country.formatted_address;

        };


        var body = {
            name: 'Random name',
            lat: generateRandomPointOnRadius({lat: this.props.maps.getCenter().lat(), lng: this.props.maps.getCenter().lng()}, 80000).lat,
            lng: generateRandomPointOnRadius({lat: this.props.maps.getCenter().lat(), lng: this.props.maps.getCenter().lng()}, 80000).lng,
            country: 'Random',
            address: 'random',
            city: 'random'
        };

        this
            .reverseGeocode({lat: body.lat, lng: body.lng})
            .then(function (results) {
                body.city = generateCity(results).long_name;
                body.address = generateAddress(results);
                body.country = generateCountry(results);

                console.info('random marker has been added', body);
                self.addMarker(body, self.props.maps);
            })
            .catch(function (results) {
                console.warn(results);
            });

    },
    getXMLdatas: function (map) {

        var self = this;

        function featching() {

            var deffered = q.defer();

            d3.xml('datas.xml', function (xml) {

                var customers = xml.getElementsByTagName('customer');
                for (var i = 0, l = customers.length; i < l; i++) {

                    var customer = customers[i];
                    var name = customer.getElementsByTagName('name')[0].textContent;
                    var lat = customer.getElementsByTagName('latitude')[0].textContent;
                    var lng = customer.getElementsByTagName('longitude')[0].textContent;
                    var address = customer.getElementsByTagName('address')[0].textContent;
                    var country = customer.getElementsByTagName('country')[0].textContent;
                    var city = customer.getElementsByTagName('city')[0].textContent;

                    var body = {
                        name: name,
                        lat: lat,
                        lng: lng,
                        address: address,
                        country: country,
                        city: city
                    };

                    self.addMarker(body, map);
                    if (i == customers.length - 1) {
                        deffered.resolve();
                    }
                }

            });

            return deffered.promise;

        }

        q.all([featching()]).then(function () {
            self.props.updateMapDataFetchState(true);
        });

    },
    componentDidMount: function () {

        var map = new google.maps.Map(d3.select('#maps').node(), {
            zoom: 9,
            center: new google.maps.LatLng(-6.840253, 107.459403),
            zoomControl: true,
            mapTypeControl: false,
            streetViewControl: false
        });

        google.maps.event.addListener(map, 'click', this.addClickListener);
        google.maps.event.addDomListener(window, 'resize', function () {
            google.maps.event.trigger(map, 'resize');
        });

        if (navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (pos) {
                var lat = pos.coords.latitude;
                var lng = pos.coords.longitude;

                map.setCenter(new google.maps.LatLng(lat, lng));
            });
        }

        this.props.setMap(map);
        this.getXMLdatas(map);
        this.setInterval(this.getRandomMarkers, 10000);
    },
    render: function () {
        return (
            <div className="MapContainer" id="maps"></div>
        );
    }
});
