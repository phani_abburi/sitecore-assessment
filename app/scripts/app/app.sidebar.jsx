'use strict';

var React = require('react');
var LineChart = require('./app.lineChart');
var PieChart = require('./app.pieChart');

module.exports = React.createClass({
    displayName: 'Sidebar',
    render: function () {
        return (
            <div className={`sidebar ${this.props.sideBarOpened ? '' : 'hide'}`}>
                <div className="sidebarContent">
                    <section className="content">
                        <PieChart statisticCharts={this.props.statisticCharts}/>
                    </section>
                    <section className="content">
                        <LineChart markers={this.props.markers} annualCharts={this.props.annualCharts} addChart={this.props.addChart}/>
                    </section>
                    <footer className="footer">
                        &copy; Sitecore, 2016
                    </footer>
                </div>
            </div>
        );
    }
});
