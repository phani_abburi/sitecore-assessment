'use strict';

var React = require('react');
var d3 = require('d3');

var getCurrentViewPort = function () {
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight || e.clientHeight || g.clientHeight;

    return {
        width: x,
        height: y
    }
}();

var pie,
    color,
    arc,
    svg,
    path,
    width = (getCurrentViewPort.width <= 375) ? 300 : 400,
    height = (getCurrentViewPort.width <= 375) ? 300 : 400;

module.exports = React.createClass({
    displayName: 'PieChart',
    getInitialState: function () {
        return {
            selected: 'Indonesia'
        }
    },
    handleChange: function (e) {
        this.setState({selected: e.target.value});
        if (this.props.statisticCharts.length > 0) {
            this.init({statisticCharts: this.props.statisticCharts}, e.target.value);
            document.getElementById('pie-chart-notifications').innerHTML = '';
        }
    },
    componentWillReceiveProps: function (nextProps) {
        if (nextProps.statisticCharts.length > 0) {
            this.init(nextProps, this.state.selected);
        }
    },
    init: function (nextProps, country) {
        function arcTween(d) {
            var i = d3.interpolate(this._current, d);
            this._current = i(0);
            return function (t) {
                return arc(i(t));
            };
        }

        if (nextProps.statisticCharts.length > 0) {

            var radius = Math.min(width, height) / 2;
            var getCountries, datas;

            if (!pie) {

                color = d3.scale.category20();
                pie = d3.layout.pie()
                    .value(function (d) {
                        return d.sum
                    });

                arc = d3.svg.arc()
                    .innerRadius(radius - 100)
                    .outerRadius(radius - 20);

                svg = d3.select('svg')
                    .attr('width', width)
                    .attr('height', height)
                    .append('g')
                    .attr("transform", "translate(" + width / 2 + "," + width / 2 + ")");

                getCountries = nextProps.statisticCharts.map(function (e) {
                    return e.country
                }).indexOf(country);
                datas = nextProps.statisticCharts[getCountries].cities.map(function (e) {
                    return e
                });

                svg.datum(datas).selectAll("path").data(pie).transition().duration(1000).attrTween("d", arcTween);
                svg.datum(datas).selectAll("path")
                    .data(pie)
                    .enter().append("path")
                    .on('mouseover', this.handleMouseOver)
                    .on("mouseout", this.handleMouseOut)
                    .attr("fill", function (d, i) {
                        return color(i);
                    })
                    .attr("d", arc)
                    .each(function (d) {
                        this._current = d;
                    }); // store the initial angles

                svg.datum(datas).selectAll('path').data(pie).exit().remove();

            } else {

                getCountries = nextProps.statisticCharts.map(function (e) {
                    return e.country
                }).indexOf(country);
                datas = nextProps.statisticCharts[getCountries].cities.map(function (e) {
                    return e
                });

                //console.log(this.state.selected);
                //console.log(nextProps.statisticCharts[getCountries])
                //console.log(datas);

                svg.datum(datas).selectAll("path").data(pie).transition().duration(1000).attrTween("d", arcTween);
                svg
                    .datum(datas)
                    .selectAll("path")
                    .data(pie)
                    .enter().append("path")
                    .on('mouseover', this.handleMouseOver)
                    .on("mouseout", this.handleMouseOut)
                    .attr("fill", function (d, i) {
                        return color(i);
                    })
                    .attr("d", arc)
                    .each(function (d) {
                        this._current = d;
                    }); // store the initial angles

                svg.datum(datas).selectAll('path').data(pie).exit().remove();

            }

        }
    },
    handleMouseOver: function (e) {
        var content = e.data.name + ' : ' + e.data.sum + ' user(s)';
        document.getElementById('pie-chart-notifications').innerHTML = content;
    },
    handleMouseOut: function (e) {
        //document.getElementById('pie-chart-notifications').innerHTML = '';
    },
    render: function () {
        var options = this.props.statisticCharts.map(function (option) {
            return (
                <option value={option.country}>{option.country}</option>
            )
        });
        return (
            <div>
                <h4 className="title-section">Realtime statistical report</h4>
                <div className="form-group">
                    <label>Select country</label>
                    <select name="country" className="form-control pie-chart select-box" value={this.state.selected} onChange={this.handleChange.bind(this)}>
                        {options}
                    </select>
                </div>
                <svg className="pie-chart"></svg>
                <div id="pie-chart-notifications"></div>
            </div>
        );
    }
});


