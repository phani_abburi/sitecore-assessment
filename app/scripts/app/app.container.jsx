'use strict';

var React = require('react');
var MapContainer = require('./app.map');
var TopBar = require('./app.topbar');
var SideBar = require('./app.sidebar');
var NotificationsContainer = require('./app.notificationsContainer');
var d3 = require('d3');

module.exports = React.createClass({
    displayName: 'Container',
    getInitialState: function () {
        return {
            sideBarOpened: this.handleResize,
            maps: {},
            markers: [],
            annualCharts: [],
            statisticCharts: [],
            mapDataFetched: false
        }
    },
    componentDidMount: function () {
        window.addEventListener('resize', this.handleResize);
    },
    componentWillUnmount: function () {
        window.removeEventListener('resize', this.handleResize);
    },
    handleResize: function (e) {
        this.setState({sideBarOpened: (window.innerWidth >= 426) ? true : false});
    },
    toggleOpen: function () {
        this.setState({sideBarOpened: !this.state.sideBarOpened});
    },
    addMarker: function (obj) {
        this.setState({
            markers: this.state.markers.concat([obj])
        });
        this.processBulkDatas(obj);
    },
    addChart: function (obj) {
        this.setState({
            annualCharts: this.state.annualCharts.concat([obj])
        });
    },
    addMarkerMetricsData: function (obj) {
        this.setState({
            statisticCharts: this.state.statisticCharts.concat([obj])
        });
    },
    setMap: function (obj) {
        this.setState({maps: obj});
    },
    processBulkDatas: function (obj) {
        var props = obj.properties;
        var body = {};

        if (this.state.statisticCharts.length > 0) {

            /**
             * if there are registered countries
             * */

            var existingCountries = this.state.statisticCharts.map(function (e) {
                return e.country
            }).indexOf(props.country);

            /**
             * if the country already registered
             * */

            if (existingCountries > -1) {

                var addingNewCountrySum = this.state.statisticCharts[existingCountries].sum + 1;
                var existingCity = this.state.statisticCharts[existingCountries].cities.map(function (e) {
                    return e.name
                }).indexOf(props.city);

                /**
                 * if the city already registered
                 * */

                if (existingCity > -1) {

                    var addingNewCitySum = this.state.statisticCharts[existingCountries].cities[existingCity].sum + 1;
                    body = {
                        country: this.state.statisticCharts[existingCountries].country,
                        sum: addingNewCountrySum,
                        cities: this.state.statisticCharts[existingCountries].cities
                    };

                    body.cities[existingCity].sum = addingNewCitySum;

                } else {

                    /**
                     * if the city hasn't registered yet
                     * */

                    body = {
                        country: this.state.statisticCharts[existingCountries].country,
                        sum: addingNewCountrySum,
                        cities: this.state.statisticCharts[existingCountries].cities
                    };

                    body.cities.push({
                        name: props.city,
                        sum: 1
                    });

                }

                /**
                 * remove the old array and replace it with the new one
                 * */

                for (var i = this.state.statisticCharts.length - 1; i >= 0; i--) {
                    if (this.state.statisticCharts[i].country == props.country) {
                        this.state.statisticCharts.splice(i, 1);
                    }
                }

                this.addMarkerMetricsData(body);

            } else {

                /**
                 * if the country hasn't registered yet
                 * */

                body = {
                    country: props.country,
                    sum: 1,
                    cities: [
                        {
                            name: props.city,
                            sum: 1
                        }
                    ]
                };

                this.addMarkerMetricsData(body);

            }

        } else {

            /**
             * if there are no countries being registered yet
             * */

            body = {
                country: props.country,
                sum: 1,
                cities: [
                    {
                        name: props.city,
                        sum: 1
                    }
                ]
            };

            this.addMarkerMetricsData(body);

        }

    },
    updateMapDataFetchState: function(bool) {
        this.setState({mapDataFetched:bool});
    },
    render: function () {
        return (
            <div className="app-container">
                <div className="navigations-container">
                    <TopBar sideBarOpened={this.state.sideBarOpened} toggleOpen={this.toggleOpen}/>
                </div>
                <div className="view-container">
                    <MapContainer sideBarOpened={this.state.sideBarOpened} maps={this.state.maps} markers={this.state.markers}
                                  addNewMarker={this.addMarker} setMap={this.setMap} updateMapDataFetchState={this.updateMapDataFetchState}/>
                    <SideBar sideBarOpened={this.state.sideBarOpened} markers={this.state.markers} annualCharts={this.state.annualCharts}
                             addChart={this.addChart} statisticCharts={this.state.statisticCharts}/>
                </div>
                <NotificationsContainer mapDataFetched={this.state.mapDataFetched}/>
            </div>
        );
    }
});
