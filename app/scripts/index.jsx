'use strict';

var ReactDom = require('react-dom');
var React = require('react');

var Container = require('./app/app.container');

ReactDom.render(<Container />, document.getElementById('content'));
